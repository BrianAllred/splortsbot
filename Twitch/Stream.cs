using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using TwitchLib.Api;
using TwitchLib.Api.Services;
using TwitchLib.Api.Services.Events.LiveStreamMonitor;

namespace SplortsBot.Twitch
{
    public class Stream : IDisposable
    {
        const string TwitchStreamUrl = "twitch.tv/ch00beh";

        private readonly Process streamlinkProcess;
        private readonly ProcessStartInfo streamlinkStartInfo;
        private readonly Process ffmpegProcess;
        private readonly ProcessStartInfo ffmpegStartInfo;
        private readonly IAudioClient audioClient;
        private readonly IVoiceChannel channel;
        private readonly CancellationTokenSource redirectTokenSource;
        private readonly CancellationTokenSource streamTokenSource;

        public Stream(IAudioClient audioClient, IVoiceChannel channel)
        {
            this.streamlinkStartInfo = new ProcessStartInfo
            {
                FileName = "streamlink",
                Arguments = $"--no-version-check --twitch-disable-hosting --twitch-disable-ads --twitch-low-latency -O {TwitchStreamUrl} audio,audio_only",
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            this.streamlinkProcess = new Process
            {
                StartInfo = this.streamlinkStartInfo
            };

            this.ffmpegStartInfo = new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = "-hide_banner -loglevel panic -i - -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            this.ffmpegProcess = new Process
            {
                StartInfo = ffmpegStartInfo
            };

            this.redirectTokenSource = new CancellationTokenSource();
            this.streamTokenSource = new CancellationTokenSource();
            this.audioClient = audioClient;
            this.channel = channel;
        }

        public async Task StartStream()
        {
            var twitchApi = new TwitchAPI();
            twitchApi.Settings.AccessToken = Environment.GetEnvironmentVariable("SPLORTS_TWITCH_ACCESS");
            twitchApi.Settings.ClientId = Environment.GetEnvironmentVariable("SPLORTS_TWITCH_CLIENTID");
            twitchApi.Settings.Secret = Environment.GetEnvironmentVariable("SPLORTS_TWITCH_SECRET");
            var streamMonitor = new LiveStreamMonitorService(twitchApi);
            streamMonitor.SetChannelsByName(new List<string> { TwitchStreamUrl.Substring(TwitchStreamUrl.IndexOf('/') + 1) });

            streamMonitor.OnStreamOnline += StreamOnline;
            streamMonitor.OnStreamOffline += StreamOffline;
            streamMonitor.Start();
            await streamMonitor.UpdateLiveStreamersAsync();

            await Task.Delay(Timeout.Infinite, streamTokenSource.Token);
        }

        private void StreamOffline(object sender, OnStreamOfflineArgs e)
        {
            this.Dispose();
        }

        private void StreamOnline(object sender, OnStreamOnlineArgs e)
        {
            Console.WriteLine("Starting processes");
            this.streamlinkProcess.Start();
            this.ffmpegProcess.Start();

            Console.WriteLine("Copying streamlink stdout to ffmpeg stdin");
            this.streamlinkProcess.StandardOutput.BaseStream.CopyToAsync(this.ffmpegProcess.StandardInput.BaseStream, this.redirectTokenSource.Token);

            Task.Run(SendVoice);
        }

        private async Task SendVoice()
        {
            using (var voiceChat = audioClient.CreatePCMStream(AudioApplication.Voice))
            {
                try
                {
                    Console.WriteLine("Sending audio to Discord");
                    await this.ffmpegProcess.StandardOutput.BaseStream.CopyToAsync(voiceChat);
                }
                finally
                {
                    Console.WriteLine("Disposing of stream");
                    this.Dispose();
                    await voiceChat.FlushAsync();
                }
            }
        }

        private void KillProcesses()
        {
            // This makes the bot non-crossplatform, but process.Kill() doesn't seem to be doing the trick...
            Process.Start(new ProcessStartInfo
            {
                FileName = "kill",
                Arguments = $"-kill {this.streamlinkProcess.Id} {this.ffmpegProcess.Id}"
            });
        }

        public void Dispose()
        {
            try
            {
                this.channel.DisconnectAsync();
                this.redirectTokenSource.Cancel();
                this.KillProcesses();
            }
            finally
            {
                this.streamTokenSource.Cancel();
            }
        }
    }
}