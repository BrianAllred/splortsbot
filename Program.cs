﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using SplortsBot.Discord;

namespace SplortsBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            EstablishAsyncContext(args);
        }

        public static void EstablishAsyncContext(string[] args)
        {
            try
            {
                new Program().MainAsync(args).GetAwaiter().GetResult();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                EstablishAsyncContext(args);
            }
        }

        public async Task MainAsync(string[] args)
        {
            var token = args.Length > 0 ? args[0] : null;

            using (var services = ConfigureServices())
            {
                Console.WriteLine("Setting up client");
                var client = services.GetRequiredService<DiscordSocketClient>();
                client.Log += LogMessage;
                await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("SPLORTS_BOT_TOKEN") ?? token);
                await client.StartAsync();

                client.Ready += async () =>
                {
                    await client.CurrentUser.ModifyAsync(user => user.Avatar = new Image("Assets/avatar.jpg"));
                    await client.SetGameAsync("!splorts help");
                };

                Console.WriteLine("Client logged in");

                await services.GetRequiredService<CommandHandler>().InitializeAsync();

                Console.WriteLine("Command services initialized");

                await Task.Delay(Timeout.Infinite);
            }
        }

        private ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandHandler>()
                .AddSingleton<CommandService>()
                .BuildServiceProvider();
        }

        private Task LogMessage(LogMessage message)
        {
            Console.WriteLine(message.Message);

            return Task.CompletedTask;
        }
    }
}
