using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Audio;
using Discord.Commands;
using SplortsBot.Twitch;

namespace SplortsBot.Discord
{
    [Group("splorts")]
    public class StreamModule : ModuleBase<SocketCommandContext>
    {
        private CommandService commandService;

        public StreamModule(CommandService commandService)
        {
            this.commandService = commandService;
        }

        [Command("join", RunMode = RunMode.Async)]
        [Summary("Invite SplortsBot into current or specified voice channel. Feel free to re-issue this command if the bot crashes or audio stops.")]
        public async Task JoinChannel(
            [Summary("Specific voice channel to join (optional if user currently in a voice channel).")]
            IVoiceChannel channel = null
            )
        {
            // Get the audio channel
            channel = channel ?? (Context.User as IGuildUser)?.VoiceChannel;
            if (channel == null) { await Context.Channel.SendMessageAsync("User must be in a voice channel, or a voice channel must be passed as an argument."); return; }

            // For the next step with transmitting audio, you would want to pass this Audio Client in to a service.
            Console.WriteLine("Connecting to voice channel");
            var audioClient = await channel.ConnectAsync();

            using (var stream = new Stream(audioClient, channel))
            {
                await stream.StartStream();
            }
        }

        [Command("help")]
        [Alias("listcommands")]
        [Summary("Lists SplotsBot's available commands.")]
        public async Task ListCommandsAsync()
        {
            var commands = commandService.Commands.Where(command => command.Module.Group == "splorts");
            var embedBuilder = new EmbedBuilder();

            embedBuilder.WithTitle("SplortsBot Help");

            foreach (var command in commands)
            {
                var embedFieldName = $"`!splorts {command.Name}`";
                var embedFieldText = command.Summary + "\n" ?? "No description available.\n";

                if (command.Parameters.Count == 0)
                {
                    embedFieldText += "\tNo parameters.\n";
                }
                else
                {
                    foreach (var parameter in command.Parameters)
                    {
                        var paramSummary = parameter.Summary ?? "No description available.\n";
                        embedFieldText += $"\t`{parameter.Name}`: {parameter.Summary}\n";
                    }
                }

                embedBuilder.AddField(embedFieldName, embedFieldText);
            }

            await ReplyAsync("Here's a list of commands and their descriptions: ", false, embedBuilder.Build());
        }

    }
}